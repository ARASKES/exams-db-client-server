CREATE DATABASE exam;
\c exam;

CREATE SCHEMA exam_schema;

CREATE TABLE exam_schema.student(id INTEGER, lastName TEXT, firstName TEXT, birthDate DATE, idBook INTEGER);

CREATE TABLE exam_schema.recordbook(idBook INTEGER,subjectName TEXT,teacherName TEXT,examDate DATE,mark INTEGER);

INSERT INTO exam_schema.student VALUES (1, 'Рыжак','Андрей','2002-04-11',1);
INSERT INTO exam_schema.student VALUES (2, 'Бойко','Алексей','2001-04-03',2);
INSERT INTO exam_schema.student VALUES (3, 'Кузнецов','Валерий','2001-06-10',3);
INSERT INTO exam_schema.student VALUES (4, 'Зернова','Адреана','2002-09-13',4);
INSERT INTO exam_schema.student VALUES (5, 'Чайка','Егор','2002-06-08',5);

INSERT INTO exam_schema.recordbook VALUES (1, 'Инструментальный анализ защищенности','Магомедов Ш.Г','2023-01-10',5);
INSERT INTO exam_schema.recordbook VALUES (1, 'Модели развёртывания','Сачков В.Е.','2023-01-14',5);
INSERT INTO exam_schema.recordbook VALUES (1, 'Обработка потоковой информации интернет-вещей','Котилевец И.Д.','2023-01-18',5);

INSERT INTO exam_schema.recordbook VALUES (2, 'Инструментальный анализ защищенности','Магомедов Ш.Г','2023-01-10',5);
INSERT INTO exam_schema.recordbook VALUES (2, 'Модели развёртывания','Сачков В.Е.','2023-01-14',5);
INSERT INTO exam_schema.recordbook VALUES (2, 'Обработка потоковой информации интернет-вещей','Котилевец И.Д.','2023-01-18',5);

INSERT INTO exam_schema.recordbook VALUES (3, 'Инструментальный анализ защищенности','Магомедов Ш.Г','2023-01-10',5);
INSERT INTO exam_schema.recordbook VALUES (3, 'Модели развёртывания','Сачков В.Е.','2023-01-14',5);
INSERT INTO exam_schema.recordbook VALUES (3, 'Обработка потоковой информации интернет-вещей','Котилевец И.Д.','2023-01-18',5);

INSERT INTO exam_schema.recordbook VALUES (4, 'Инструментальный анализ защищенности','Магомедов Ш.Г','2023-01-10',5);
INSERT INTO exam_schema.recordbook VALUES (4, 'Модели развёртывания','Сачков В.Е.','2023-01-14',5);
INSERT INTO exam_schema.recordbook VALUES (4, 'Обработка потоковой информации интернет-вещей','Котилевец И.Д.','2023-01-18',5);

INSERT INTO exam_schema.recordbook VALUES (5, 'Инструментальный анализ защищенности','Магомедов Ш.Г','2023-01-10',5);
INSERT INTO exam_schema.recordbook VALUES (5, 'Модели развёртывания','Сачков В.Е.','2023-01-14',5);
INSERT INTO exam_schema.recordbook VALUES (5, 'Обработка потоковой информации интернет-вещей','Котилевец И.Д.','2023-01-18',5);
