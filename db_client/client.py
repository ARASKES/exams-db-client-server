import psycopg2
import os
import time
import subprocess

time.sleep(30)

conn = psycopg2.connect(dbname='exam', user='postgres',
                        password='postgres', host='db', port=5432)
cursor = conn.cursor()

OUTPUT_PATH = "output"

cursor.execute('select idBook, examDate from exam_schema.recordbook ORDER BY idBook;')
records = cursor.fetchall()

os.makedirs(OUTPUT_PATH, exist_ok=True)
with open(os.path.join(OUTPUT_PATH, "output.txt"), "w+") as file:
    for i in range(len(records)):
        print(f'{records[i][0]} {records[i][1]}')
        file.write(f'{records[i][0]} {records[i][1]}\n')

result = subprocess.run(['ls', OUTPUT_PATH], stdout=subprocess.PIPE)
print(result.stdout)

cursor.close()
conn.close()
