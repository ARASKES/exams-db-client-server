FROM python:latest

USER 0

RUN apt-get update

RUN python3 -m pip install psycopg2

ARG PROJECT=client
ARG PROJECT_DIR=/${PROJECT}
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR

COPY /db_client/client.py $PROJECT_DIR

CMD ["python3", "client.py"]
